const values = require('../values.js')
const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    fullName: function () {
        return this.firstName + " " + this.lastName;
    }
};

console.log(values(testObject))