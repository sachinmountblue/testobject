const mapObject = require('../mapObject.js')
const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};



console.log(mapObject(testObject, cb));


// callback function that map every property of obj to new value

function cb(previousKey) {
    return "new " + previousKey;
}