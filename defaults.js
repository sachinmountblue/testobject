function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults

    for (let i in defaultProps) {
        if (!obj.hasOwnProperty(defaultProps[i])) {
            obj[i] = defaultProps[i]
        }

    }
    return obj

}
module.exports = defaults