function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert

    let keyArr = [...Object.keys(obj)]
    let valueArr = [...Object.values(obj)]

    let newObj = {}
    for (let i = 0; i < valueArr.length; i++) {
        newObj[valueArr[i]] = keyArr[i]

    }
    return newObj

}

module.exports = invert