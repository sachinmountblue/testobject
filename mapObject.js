function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    const newMapObject = {}
    for (let key in obj) {
        newMapObject[key] = cb(obj[key]);
    }
    return newMapObject;
}

module.exports = mapObject